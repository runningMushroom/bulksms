﻿using System;

namespace BulkSms
{
    public interface IBulkSms
    {
        void SendSingle(SmsMessageDto message);
    }
}