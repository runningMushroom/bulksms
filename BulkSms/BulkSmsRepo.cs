﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace BulkSms
{
    public class BulkSmsRepo : IBulkSms
    {
        private readonly BulksSmsSettingsDto _settings;
        private readonly ILogger _logger;

        public BulkSmsRepo(BulksSmsSettingsDto settings, ILogger logger)
        {
            _settings = settings;
            _logger = logger;
        }

        public void SendSingle(SmsMessageDto message)
        {
            try
            {
                // Create new Restsharp Http client
                var client = new RestClient(string.Format(_settings.BaseUrl));

                // create the request
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Authorization", _settings.ApiToken);
                request.AddJsonBody(new StringContent(JsonConvert.SerializeObject(message), Encoding.UTF8, "application/json"));

                // send the request
                IRestResponse response = client.Execute(request);

                // throw error if the response is not successfull
                if (!response.IsSuccessful)
                    throw new Exception($"Request StatusCode:{response.StatusCode}\nRequest Content{response.Content}");
            }
            catch (Exception e)
            {
                _logger.LogError($"BulkSmsRepo.SendSingle Error: {e.Message}");
            }
        }
    }
}