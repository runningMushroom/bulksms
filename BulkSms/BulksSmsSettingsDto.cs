﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BulkSms
{
    public class BulksSmsSettingsDto
    {
        public string BaseUrl { get; set; }

        // base64 string of username:password - see bulkSMS documentation
        public string ApiToken { get; set; }
    }
}