﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace BulkSms
{
    public static class BulkSmsExtension
    {
        public static void ConfigureBulkSms(this IServiceCollection services, IConfiguration config)
        {
            services.AddSingleton(config.GetSection("BulkSmsSettings").Get<BulksSmsSettingsDto>());
            services.AddSingleton<IBulkSms, BulkSmsRepo>();
        }
    }
}