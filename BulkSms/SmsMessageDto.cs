﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BulkSms
{
    public class SmsMessageDto
    {
        public string body { get; set; }
        public string to { get; set; }
        public string encoding { get; set; }

        public SmsMessageDto()
        {
            encoding = "TEXT";
        }
    }
}