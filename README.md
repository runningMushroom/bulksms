# Functionality
* send sms to single user via bulksms

# Target Framework
* netcoreapp3.1

# Instructions
* Add your bulksms paramaters to appsettings.json
	* "BulkSmsSettings": {"BaseUrl": "https://urlbybulksms", "ApiToken": "your api token"}
* Add services.ConfigureBulkSms(Configuration) to your Startup.cs file.
